while true; do
    clear  # Clear the terminal

        # CPU Usage
	    echo "CPU Usage:"
	        top -n 1 | grep "%Cpu(s)" | awk '{print "  User: " $2, "System: " $4, "Idle: " $8}'
		    echo

		        # Memory Usage
			    echo "Memory Usage:"
			        free -m | awk 'NR==2{print "  Total: " $2 " MB", "Used: " $3 " MB", "Free: " $4 " MB"}'
				    echo

				        # Disk Space
					    echo "Disk Space:"
					        df -h | awk '$NF=="/"{print "  Total: " $2, "Used: " $3, "Available: " $4, "Usage: " $5}'
						    echo

						        # Sleep for a few seconds before updating
							    sleep 1
						    done

